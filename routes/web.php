<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('pages.login.login');
});

Route::get('/students', 'StudentsController@index');
Route::get('/students/create', 'StudentsController@create');
Route::get('/students/{student}', 'StudentsController@show');
Route::post('/students', 'StudentsController@store');
Route::delete('/students/{student}', 'StudentsController@destroy');
Route::get('/students/{student}/edit', 'StudentsController@edit');
Route::patch('/students/{student}', 'StudentsController@update');

Route::get('/tes', 'StudentsController@tes');

Route::get('/costs', 'CostsController@index');
Route::get('/costs/create', 'CostsController@create');
Route::post('/costs', 'CostsController@store');
Route::delete('/costs/{cost}', 'CostsController@destroy');
Route::get('/costs/{cost}/edit', 'CostsController@edit');
Route::patch('/costs/{cost}', 'CostsController@update');

Route::get('/classes', 'ClassesController@index');
Route::get('/classes/create', 'ClassesController@create');
Route::post('/classes', 'ClassesController@store');
Route::delete('/classes/{classes}', 'ClassesController@destroy');
Route::get('/classes/{classes}/edit', 'ClassesController@edit');
Route::patch('/classes/{classes}', 'ClassesController@update');


Route::get('/majors', 'MajorsController@index');
Route::get('/majors/create', 'MajorsController@create');
Route::post('/majors', 'MajorsController@store');
Route::delete('/majors/{major}', 'MajorsController@destroy');
Route::get('/majors/{major}/edit', 'MajorsController@edit');
Route::patch('/majors/{major}', 'MajorsController@update');
