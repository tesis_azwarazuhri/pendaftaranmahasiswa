<?php

namespace App\Http\Controllers;

use App\Classes;
use Illuminate\Http\Request;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $classes = Classes::all();
        // $jurusan = Major::all();
        // return view('pages/dataentry/indexstudents', compact('mahasiswa','jurusan'));
        return view('pages/kelas/indexkelas', compact('classes'));
    }

    public function create()
    {
        //
        return view('pages/kelas/createkelas');
    }


    public function store(Request $request)
    {
        //
        $request->validate([
            'kelas'=>'required',
            'uangSpp'=>'required',
        ]);
        // try {
            Classes::create([
                'kelas' => $request->kelas,
                'uang_spp' => $request->uangSpp,
            ]);

            return redirect('/classes')->with('status','Berhasil Menambahkan Data Kelas dan Spp');

        // } catch(\Illuminate\Database\QueryException $e){
        //     $errorCode = $e->errorInfo[1];
        //     if($errorCode == '1062'){
        //         dd('Gelombang Telah Terdaftar');
        //         // return redirect('/students/create')->with('status','Email/No tlp telah terdaftar');

        //     }
        // }
    }

    public function destroy(Classes $classes)
    {
        //
        Classes::destroy($classes->id);
        return redirect('/classes')->with('status','Berhasil Menghapus Data Kelas dan SPP');
    }


    public function edit(Classes $classes)
    {
        //
        return view('pages/kelas/editkelas', compact('classes'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cost  $cost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classes $classes)
    {
        //
        $request->validate([
            'kelas'=>'required',
            'UangSpp'=>'required',
        ]);
        // try {
            // Classes::create([
            //     'kelas' => 'azwar',
            //     'uang_spp' => '95',
            // ]);

            Classes::where('id',$classes->id)->update([
                'kelas' => $request->kelas,
                'uang_spp' => $request->uangSpp,
            ]);
            
            return redirect('/classes')->with('status','Berhasil Merubah Data Kelas dan SPP');
    }
}
