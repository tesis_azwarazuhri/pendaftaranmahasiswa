<?php

namespace App\Http\Controllers;

use App\Cost;
use Illuminate\Http\Request;

class CostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $biaya = Cost::all();
        // $jurusan = Major::all();
        // return view('pages/dataentry/indexstudents', compact('mahasiswa','jurusan'));
        return view('pages/biaya/indexbiaya', compact('biaya'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages/biaya/createbiaya');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'gelombang'=>'required',
            'uangPendaftaran'=>'required',
            'uangPendaftaran'=>'required'
        ]);
        // try {
            Cost::create([
                'gelombang' => $request->gelombang,
                'uang_pendaftaran' => $request->uangPendaftaran,
                'uang_gedung' => $request->uangGedung,
            ]);

            return redirect('/costs')->with('status','Berhasil Menambahkan Data Biaya');

        // } catch(\Illuminate\Database\QueryException $e){
        //     $errorCode = $e->errorInfo[1];
        //     if($errorCode == '1062'){
        //         dd('Gelombang Telah Terdaftar');
        //         // return redirect('/students/create')->with('status','Email/No tlp telah terdaftar');

        //     }
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cost  $cost
     * @return \Illuminate\Http\Response
     */
    public function show(Cost $cost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cost  $cost
     * @return \Illuminate\Http\Response
     */
    public function edit(Cost $cost)
    {
        //
        return view('pages/biaya/editbiaya', compact('cost'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cost  $cost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cost $cost)
    {
        //
        $request->validate([
            'gelombang'=>'required',
            'uangPendaftaran'=>'required',
            'uangPendaftaran'=>'required'
        ]);
        // try {
            // Cost::create([
            //     'gelombang' => $request->gelombang,
            //     'uang_pendaftaran' => $request->uangPendaftaran,
            //     'uang_gedung' => $request->uangGedung,
            // ]);

               //
            Cost::where('id',$cost->id)->update([
                'gelombang' => $request->gelombang,
                'uang_pendaftaran' => $request->uangPendaftaran,
                'uang_gedung' => $request->uangGedung,
            ]);
            return redirect('/costs')->with('status','Berhasil Merubah Data Biaya');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cost  $cost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cost $cost)
    {
        //
        Cost::destroy($cost->id);
        return redirect('/costs')->with('status','Berhasil Menghapus Data Mahasiswa');
    }
}
