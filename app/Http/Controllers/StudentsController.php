<?php

namespace App\Http\Controllers;

use App\Classes;
use App\Cost;
use App\Major;
use App\Student;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mahasiswa = Student::all();
        // $jurusan = Major::all();
        // return view('pages/dataentry/indexstudents', compact('mahasiswa','jurusan'));
        return view('pages/dataentry/indexstudents', compact('mahasiswa'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // $jurusan = Major::all();
        // $jurusan = Major::select('kode_jurusan','nama_jurusan')->get();
        // return view('pages.dataentry.create',compact('jurusan'));

        $jurusan = Major::select('kode_jurusan','nama_jurusan','id')->get();
        $kelas = Classes::select('kelas','uang_spp','id')->get();
        $gelombang = Cost::select('gelombang','id')->get();
        return view('pages/dataentry/newcreate',compact('jurusan','kelas','gelombang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $count = Student::count();   
        
        $request->validate([
            'nama'=>'required',
            'tempatLahir'=>'required',
            'tglLahir'=>'required',
            'alamat'=>'required',
            'noTlp'=>'required',
            'jurusan'=>'required',
            'email'=>'required',
            'asalSekolah'=>'required'
        ]);

        try {
        Student::create([
            'kode_pendaftaran' => "11190".$count.$request->jurusan,
            // 'kode_pendaftaran' => "11190".$request->jurusan,
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempatLahir,
            'tgl_lahir' => $request->tglLahir,
            'alamat' => $request->alamat,
            'asal_sekolah' => $request->asalSekolah,
            'no_tlp' => $request->noTlp,
            'email' => $request->email,
            'jurusan' => $request->jurusan,
            'kelas_id' => $request->kelas_id,
            'gelombang_id' => $request->gelombang_id,
            ]);

             return redirect('/students')->with('status','Berhasil Menambahkan Data Mahasiswa');
        } catch(\Illuminate\Database\QueryException $e){
            $errorCode = $e->errorInfo[1];
            if($errorCode == '1062'){
                dd('Nomer Hape/Email telah terdaftar');
                // return redirect('/students/create')->with('status','Email/No tlp telah terdaftar');

            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //

        $jrs = Major::select('nama_jurusan')->
        where('kode_jurusan','=',$student->jurusan)->get();

        $kls = Classes::select('id','kelas','uang_spp')->where('id','=',$student->kelas_id)->get();
        $biaya = Cost::select('uang_pendaftaran', 'uang_gedung')
                ->where('gelombang','=',$student->gelombang_id)->get();

        return view('pages/dataentry/show', compact('student','jrs','kls','biaya'));
        // return $kls;


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
        // $student = Student::all();
        $jurusan = Major::select('kode_jurusan','nama_jurusan')->get();
        $kelas = Classes::select('kelas','uang_spp','id')->get();
        $gelombang = Cost::select('gelombang','id')->get();
        return view('pages/dataentry/edit', compact('student','jurusan','kelas','gelombang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $request->validate([
            'nama'=>'required',
            'tempatLahir'=>'required',
            'tglLahir'=>'required',
            'alamat'=>'required',
            'noTlp'=>'required',
            'jurusan'=>'required',
            'email'=>'required',
            'asalSekolah'=>'required'
        ]);
        //
        Student::where('id',$student->id)->update([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempatLahir,
            'tgl_lahir' => $request->tglLahir,
            'alamat' => $request->alamat,
            'asal_sekolah' => $request->asalSekolah,
            'no_tlp' => $request->noTlp,
            'email' => $request->email,
            'jurusan' => $request->jurusan,
            'kelas_id' => $request->kelas_id,
            'gelombang_id' => $request->gelombang_id,
            ]);

        return redirect('/students')->with('status','Berhasil Merubah Data Mahasiswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
        // return $student;
        Student::destroy($student->id);
        return redirect('/students')->with('status','Berhasil Menghapus Data Mahasiswa');
    }


    public function tes()
    {
        $jurusan = Major::select('kode_jurusan','nama_jurusan','id')->get();
        $kelas = Classes::select('kelas','uang_spp','id')->get();
        $gelombang = Cost::select('gelombang','id')->get();
        return view('pages/dataentry/newcreate',compact('jurusan','kelas','gelombang'));
        // return 'test';

    }
}
