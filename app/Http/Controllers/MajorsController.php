<?php

namespace App\Http\Controllers;

use App\Major;
use Illuminate\Http\Request;

class MajorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $majors = Major::all();
        // $jurusan = Major::all();
        // return view('pages/dataentry/indexstudents', compact('mahasiswa','jurusan'));
        return view('pages/jurusan/indexjurusan', compact('majors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages/jurusan/createjurusan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'kodeJurusan'=>'required',
            'namaJurusan'=>'required',
            'fakultas'=>'required'
        ]);
            Major::create([
                'kode_jurusan' => $request->kodeJurusan,
                'nama_jurusan' => $request->namaJurusan,
                'fakultas' => $request->fakultas,
            ]);

            return redirect('/majors')->with('status','Berhasil Menambahkan Data Biaya');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Major  $major
     * @return \Illuminate\Http\Response
     */
    public function show(Major $major)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Major  $major
     * @return \Illuminate\Http\Response
     */
    public function edit(Major $major)
    {
        //
        return view('pages/jurusan/editjurusan', compact('major'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Major  $major
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Major $major)
    {
        //
        $request->validate([
            'kodeJurusan'=>'required',
            'namaJurusan'=>'required',
            'fakultas'=>'required'
        ]);
           //
           Major::where('id',$major->id)->update([
            'kode_jurusan' => $request->kodeJurusan,
            'nama_jurusan' => $request->namaJurusan,
            'fakultas' => $request->fakultas,
         ]);

            return redirect('/majors')->with('status','Berhasil Merubah Data Jurusan');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Major  $major
     * @return \Illuminate\Http\Response
     */
    public function destroy(Major $major)
    {
        //
         //
         Major::destroy($major->id);
         return redirect('/majors')->with('status','Berhasil Menghapus Data Jurusan');
    }
}
