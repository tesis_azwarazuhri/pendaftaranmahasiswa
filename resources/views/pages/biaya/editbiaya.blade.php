@extends('layouts/main')

@section('title','Isi Data Calon Mahasiswa')
    
@section('container')

<div class="card card-primary ">
    <div class="card-header">
      <h3 class="card-title">Edit Data Biaya</h3>
    </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form method="POST" action="/costs/{{$cost->id}}">
                    @method('patch')
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="gelombang">Gelombang</label>
                            <input type="number" class="form-control  @error('gelombang') is-invalid @enderror" id="gelombang" placeholder="Gelombang" name="gelombang" value="{{$cost->gelombang}}">
                            @error('gelombang')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>

                        <div class="form-group">
                            <label for="uangPendaftaran">Uang Pendaftaran</label>
                            <input type="number" class="form-control  @error('uangPendaftaran') is-invalid @enderror" id="uangPendaftaran" placeholder="Rp." name="uangPendaftaran" value="{{$cost->uang_pendaftaran}}">
                            @error('uangPendaftaran')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>

                        <div class="form-group">
                            <label for="uangGedung">Uang Gedung</label>
                            <input type="number" class="form-control  @error('uangGedung') is-invalid @enderror" id="uangGedung" placeholder="Rp." name="uangGedung" value="{{$cost->uang_gedung}}">
                            @error('uangPendaftaran')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>
                </div>
                
            </div>
        </div>
</div>
@endsection