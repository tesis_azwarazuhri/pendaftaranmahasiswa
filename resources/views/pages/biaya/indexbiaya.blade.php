@extends('layouts/main')

@section('title','Pendaftaran')
    
@section('container')
<div class="row">
  <div class="col">
      <h1>Isi Data Mahasiswa </h1>
  </div>
</div>

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
   <!-- /.row -->
   <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Tambah Biaya Pendaftaran<a href="/costs/create"><i class="fas fa-plus" style="padding-left: 10px; color: red;" data-toggle="modal" data-target="#modal-xl"></i> </a></h3>
          <!-- <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl">
            Launch Extra Large Modal
          </button> -->

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 350px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0"  style="height: 300px;">
          <table class="table table-head-fixed text-nowrap">
            <thead>
              <tr>
                <th>No.</th>
                <th>Gelombang</th>
                <th>Uang Pendaftaran</th>
                <th>Uang Gedung</th>
                <th>Action</th>

              </tr>
            </thead>
            <tbody>
              {{-- {{ $jurusan }} --}}
              @foreach ($biaya as $cost)
              <tr>
                <th scope="row">{{ $loop->iteration}}</th>
                <td>{{ $cost->gelombang}}</td>
                <td>{{ $cost->uang_pendaftaran}}</td>
                <td>{{ $cost->uang_gedung}}</td>

                <td>
                  {{-- <a href="/students/1">tesss</a> --}}
                  {{-- <a href="/costs/{{$cost->id}}"><i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  <a> --}}
                  
                    <a href="/costs/{{$cost->id}}/edit"><i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  <a> 
                  
                  <form action="/costs/{{$cost->id}}" method="post" class="d-inline">
                    @method('delete')
                    @csrf
                    <button type="submit" style=" background-color: Transparent;
                    background-repeat:no-repeat;
                    border: none;
                    cursor:pointer;
                    overflow: hidden;
                    outline:none;">
                      <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                    </button>
                  </form>
              
                </td>
              </tr>  
              @endforeach
            
      
            </tbody>
          </table>
        </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
            {{-- @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif --}}
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
          </ul>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    
  </div>
  @endsection