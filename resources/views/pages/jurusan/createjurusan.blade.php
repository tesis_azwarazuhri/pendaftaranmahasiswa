@extends('layouts/main')

@section('title','Isi Data Calon Mahasiswa')
    
@section('container')

<div class="card card-primary ">
    <div class="card-header">
      <h3 class="card-title">Isi Data Jurusan</h3>
    </div>
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <form method="POST" action="/majors">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="kodeJurusan">Kode Jurusan</label>
                            <input type="" class="form-control  @error('kodeJurusan') is-invalid @enderror" id="kodeJurusan" placeholder="Kode Jurusan" name="kodeJurusan" value="{{old('kodeJurusan')}}">
                            @error('kodeJurusan')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>

                        <div class="form-group">
                            <label for="namaJurusan">Nama Jurusan</label>
                            <input type="" class="form-control  @error('namaJurusan') is-invalid @enderror" id="namaJurusan" placeholder="Nama Jurusan" name="namaJurusan" value="{{old('namaJurusan')}}">
                            @error('namaJurusan')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>

                        <div class="form-group">
                            <label for="fakultas">Fakultas</label>
                            <input type="" class="form-control  @error('fakultas') is-invalid @enderror" id="fakultas" placeholder="Fakultas" name="fakultas" value="{{old('fakultas')}}">
                            @error('fakultas')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>
                </div>
                
            </div>
        </div>
</div>
@endsection