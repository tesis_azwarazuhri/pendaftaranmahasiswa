@extends('layouts/main')

@section('title','Isi Data Calon Mahasiswa')
    
@section('container')

<br>
 <!-- general form elements -->

  <div class="card card-primary ">
    <div class="card-header">
      <h3 class="card-title">Isi Data Calon Mahasiswa</h3>
    </div>
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
   @endif
    <!-- /.card-header -->
    <!-- form start -->
    <div class="container">
      <div class="row">
        <div class="col-6">
          <form method="POST" action="/students">
              @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="nama">Nama</label>
                <input type="" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Nama Mahasiswa" name="nama" value="{{old('nama')}}">
                @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
              </div>
            
              <div class="form-group">
                  <label for="asalSekolah">Asal Sekolah</label>
                  <input type="" class="form-control @error('asalSekolah') is-invalid @enderror" id="asalSekolah" placeholder="Asal Sekolah" name="asalSekolah" value="{{old('asalSekolah')}}">
                  @error('asalSekolah')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <input type="" class="form-control  @error('alamat') is-invalid @enderror" id="alamat" placeholder="Alamat" name="alamat" value="{{old('alamat')}}">
                  @error('alamat')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>
              
                <div class="form-group">
                  <label for="noTlp">No Tlp</label>
                  <input type="number" class="form-control  @error('noTlp') is-invalid @enderror" id="noTlp" placeholder="No Tlp" name="noTlp" value="{{old('noTlp')}}">
                  @error('noTlp')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control  @error('email') is-invalid @enderror"  id="email" placeholder="Email" name="email" value="{{old('email')}}">
                  @error('email')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                <div class="form-group">
                  <label for="tempatLahir">Tempat Lahir</label>
                  <input type="" class="form-control  @error('tempatLahir') is-invalid @enderror" id="tempatLahir" placeholder="Tempat Lahir" name="tempatLahir" value="{{old('tempatLahir')}}">
                  @error('tempatLahir')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                <div class="form-group">
                  <label for="tglLahir">Tgl Lahir</label>
                  <input type="" class="form-control  @error('tglLahir') is-invalid @enderror" id="tglLahir" placeholder="Hari - Bulan - Tahun" name="tglLahir" value="{{old('tglLahir')}}">
                  @error('tglLahir')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                {{-- <div class="form-group">
                  <label for="jurusan">Jurusan</label>
                  <input type="" class="form-control @error('jurusan') is-invalid @enderror" id="jurusan" placeholder="Jurusan" name="jurusan" value="{{old('jurusan')}}">
                  @error('jurusan')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div> --}}

                <div class="form-group">
                  <label>Jurusan</label>
                  <select name="jurusan" class="form-control select2" style="width: 60%;">
                    @foreach ($jurusan as $jrs)
                    <option value="{{$jrs->kode_jurusan}}"> {{$jrs->nama_jurusan}}</option>
                    @endforeach
                  </select>
                </div>
              
              
                
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>

        <div class="col-6">
          <form method="POST" action="/students">
              @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="nama">Nama</label>
                <input type="" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Nama Mahasiswa" name="nama" value="{{old('nama')}}">
                @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
              </div>
            
              <div class="form-group">
                  <label for="asalSekolah">Asal Sekolah</label>
                  <input type="" class="form-control @error('asalSekolah') is-invalid @enderror" id="asalSekolah" placeholder="Asal Sekolah" name="asalSekolah" value="{{old('asalSekolah')}}">
                  @error('asalSekolah')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <input type="" class="form-control  @error('alamat') is-invalid @enderror" id="alamat" placeholder="Alamat" name="alamat" value="{{old('alamat')}}">
                  @error('alamat')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>
              
                <div class="form-group">
                  <label for="noTlp">No Tlp</label>
                  <input type="number" class="form-control  @error('noTlp') is-invalid @enderror" id="noTlp" placeholder="No Tlp" name="noTlp" value="{{old('noTlp')}}">
                  @error('noTlp')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control  @error('email') is-invalid @enderror"  id="email" placeholder="Email" name="email" value="{{old('email')}}">
                  @error('email')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                <div class="form-group">
                  <label for="tempatLahir">Tempat Lahir</label>
                  <input type="" class="form-control  @error('tempatLahir') is-invalid @enderror" id="tempatLahir" placeholder="Tempat Lahir" name="tempatLahir" value="{{old('tempatLahir')}}">
                  @error('tempatLahir')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                <div class="form-group">
                  <label for="tglLahir">Tgl Lahir</label>
                  <input type="" class="form-control  @error('tglLahir') is-invalid @enderror" id="tglLahir" placeholder="Hari - Bulan - Tahun" name="tglLahir" value="{{old('tglLahir')}}">
                  @error('tglLahir')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>

                {{-- <div class="form-group">
                  <label for="jurusan">Jurusan</label>
                  <input type="" class="form-control @error('jurusan') is-invalid @enderror" id="jurusan" placeholder="Jurusan" name="jurusan" value="{{old('jurusan')}}">
                  @error('jurusan')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div> --}}

                <div class="form-group">
                  <label>Jurusan</label>
                  <select name="jurusan" class="form-control select2" style="width: 60%;">
                    @foreach ($jurusan as $jrs)
                    <option value="{{$jrs->kode_jurusan}}"> {{$jrs->nama_jurusan}}</option>
                    @endforeach
                  </select>
                </div>
              
              
                
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
  <!-- /.card -->

  {{-- <div class="card card-primary col-6">
    <div class="card-header">
      <h3 class="card-title">Isi Data Calon Mahasiswa</h3>
    </div>
  </div> --}}
  </div>
 </div>


@endsection