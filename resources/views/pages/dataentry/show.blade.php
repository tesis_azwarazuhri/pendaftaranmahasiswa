@extends('layouts/main')

@section('title','Isi Data Calon Mahasiswa')
    
@section('container')

<br>


<div class="container">
    <div class="row">
            <div class="col-6">
                <div class="card-body">
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Nama Mahasiswa" name="nama" value="{{ $student->kode_pendaftaran}}" disabled>
                      @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>

                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <input type="" class="form-control  @error('alamat') is-invalid @enderror" id="alamat" placeholder="Alamat" name="alamat" value="{{ $student->alamat}}" disabled>
                      @error('alamat')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>

                    <div class="form-group">
                      <label for="noTlp">No Tlp</label>
                      <input type="number" class="form-control  @error('noTlp') is-invalid @enderror" id="noTlp" placeholder="No Tlp" name="noTlp" value="{{ $student->no_tlp}}" disabled>
                      @error('noTlp')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>

                    <div class="form-group">
                      <label for="tempatLahir">Tempat Lahir</label>
                      <input type="" class="form-control  @error('tempatLahir') is-invalid @enderror" id="tempatLahir" placeholder="Tempat Lahir" name="tempatLahir" value="{{ $student->tempat_lahir}}" disabled>
                      @error('tempatLahir')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    
                    <div class="form-group">
                      <label for="asalSekolah">Asal Sekolah</label>
                      <input type="" class="form-control @error('asalSekolah') is-invalid @enderror" id="asalSekolah" placeholder="Asal Sekolah" name="asalSekolah" value="{{ $student->asal_sekolah}}" disabled>
                      @error('asalSekolah')<div class="invalid-feedback">{{$message}}</div>@enderror
                     </div>


                     <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control  @error('email') is-invalid @enderror"  id="email" placeholder="Email" name="email" value="{{ $student->email}}" disabled>
                        @error('email')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>
                   
                      <div class="form-group">
                        <label for="gelombang">Gelombang</label>
                        <input type="gelombang" class="form-control  @error('gelombang') is-invalid @enderror"  id="gelombang" placeholder="gelombang" name="gelombang" value="{{ $student->gelombang_id}}" disabled>
                        @error('gelombang')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>

                 </div>
            </div>

            <div class="col-6">
                <div class="card-body">

                    
                    

                      <div class="form-group">
                        <label for="tglLahir">Tgl Lahir</label>
                        <input type="" class="form-control  @error('tglLahir') is-invalid @enderror" id="tglLahir" placeholder="Hari - Bulan - Tahun" name="tglLahir" value="{{ $student->tgl_lahir}}" disabled>
                        @error('tglLahir')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>

                      <div class="form-group">
                        <label for="nama_jurusan">Jurusan</label>
                        <input type="" class="form-control  @error('nama_jurusan') is-invalid @enderror" id="nama_jurusan" placeholder="Hari - Bulan - Tahun" name="nama_jurusan" value="{{ $jrs[0]->nama_jurusan}}" disabled>
                        @error('nama_jurusan')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>

                      <div class="form-group">
                        <label for="nama_jurusan">Jurusan</label>
                        <input type="" class="form-control  @error('nama_jurusan') is-invalid @enderror" id="nama_jurusan" placeholder="Hari - Bulan - Tahun" name="nama_jurusan" value="{{ $jrs[0]->nama_jurusan}}" disabled>
                        @error('nama_jurusan')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>

                      <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <input type="" class="form-control  @error('kelas') is-invalid @enderror" id="kelas" placeholder="Hari - Bulan - Tahun" name="kelas" value="{{ $kls[0]->kelas}}" disabled>
                        @error('kelas')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>

                      <div class="form-group">
                        <label for="uangPendaftaran">Uang Pendaftaran</label>
                        <input type="" class="form-control  @error('uangPendaftaran') is-invalid @enderror" id="uangPendaftaran" placeholder="Hari - Bulan - Tahun" name="uangPendaftaran" value="{{ $biaya[0]->uang_pendaftaran}}" disabled>
                        @error('uangPendaftaran')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>

                      <div class="form-group">
                        <label for="uangGedung">Uang Gedung</label>
                        <input type="" class="form-control  @error('uangGedung') is-invalid @enderror" id="uangGedung" placeholder="Hari - Bulan - Tahun" name="uangGedung" value="{{ $biaya[0]->uang_gedung}}" disabled>
                        @error('uangGedung')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>

                      <div class="form-group">
                        <label for="uangSpp">Uang SPP</label>
                        <input type="" class="form-control  @error('uangSpp') is-invalid @enderror" id="uangSpp" placeholder="Hari - Bulan - Tahun" name="uangSpp" value="{{ $kls[0]->uang_spp}}" disabled>
                        @error('uangSpp')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>

                      <div class="form-group">
                        <label for="totalBiaya">Total Biaya</label>
                        <input type="" class="form-control  @error('totalBiaya') is-invalid @enderror" id="totalBiaya" placeholder="Hari - Bulan - Tahun" name="totalBiaya" value="{{ $kls[0]->uang_spp+$biaya[0]->uang_gedung+$biaya[0]->uang_pendaftaran}}" disabled>
                        @error('uangSpp')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>
                      {{-- <div class="form-group">
                        <label>Jurusan</label>
                        <select name="jurusan" class="form-control select2" style="width: 60%;">
                          @foreach ($jurusan as $jrs)
                          <option value="{{$jrs->kode_jurusan}}"> {{$jrs->nama_jurusan}}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="form-group">
                        <label>Kelas</label>
                        <select name="kelas_id" class="form-control select2" style="width: 60%;">
                          @foreach ($kelas as $kls)
                          <option value="{{$kls->kelas}}"> {{$kls->kelas}}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="form-group">
                        <label>Gelombang</label>
                        <select name="gelombang_id" class="form-control select2" style="width: 60%;">
                          @foreach ($gelombang as $glmbng)
                          <option value="{{$glmbng->id}}"> {{$glmbng->gelombang}}</option>
                          @endforeach
                        </select>
                      </div> --}}
                 </div>
            </div>

            {{-- <div class=" col-12">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div> --}}
    </div>
</div>

@endsection