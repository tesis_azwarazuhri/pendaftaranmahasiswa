@extends('layouts/main')

@section('title','Ubah Data Calon Mahasiswa')
    
@section('container')

<br>
 <!-- general form elements -->
 <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Ubah Data Calon Mahasiswa</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="/students/{{$student->id}}">
        @method('patch')
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Nama Mahasiswa" name="nama" value="{{$student->nama}}">
          @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
        </div>
       
        <div class="form-group">
            <label for="asalSekolah">Asal Sekolah</label>
            <input type="" class="form-control @error('asalSekolah') is-invalid @enderror" id="asalSekolah" placeholder="Asal Sekolah" name="asalSekolah" value="{{$student->asal_sekolah}}">
            @error('asalSekolah')<div class="invalid-feedback">{{$message}}</div>@enderror
          </div>

          <div class="form-group">
            <label for="alamat">Alamat</label>
            <input type="" class="form-control  @error('alamat') is-invalid @enderror" id="alamat" placeholder="Alamat" name="alamat" value="{{$student->alamat}}">
            @error('alamat')<div class="invalid-feedback">{{$message}}</div>@enderror
          </div>
        
          <div class="form-group">
            <label for="noTlp">No Tlp</label>
            <input type="number" class="form-control  @error('noTlp') is-invalid @enderror" id="noTlp" placeholder="No Tlp" name="noTlp" value="{{$student->no_tlp}}">
            @error('noTlp')<div class="invalid-feedback">{{$message}}</div>@enderror
          </div>

          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control  @error('email') is-invalid @enderror"  id="email" placeholder="Email" name="email" value="{{$student->email}}">
            @error('email')<div class="invalid-feedback">{{$message}}</div>@enderror
          </div>

          <div class="form-group">
            <label for="tempatLahir">Tempat Lahir</label>
            <input type="" class="form-control  @error('tempatLahir') is-invalid @enderror" id="tempatLahir" placeholder="Tempat Lahir" name="tempatLahir" value="{{$student->tempat_lahir}}">
            @error('tempatLahir')<div class="invalid-feedback">{{$message}}</div>@enderror
          </div>

          <div class="form-group">
            <label for="tglLahir">Tgl Lahir</label>
            <input type="" class="form-control  @error('tglLahir') is-invalid @enderror" id="tglLahir" placeholder="Hari - Bulan - Tahun" name="tglLahir" value="{{$student->tgl_lahir}}">
            @error('tglLahir')<div class="invalid-feedback">{{$message}}</div>@enderror
          </div>

          {{-- <div class="form-group">
            <label for="jurusan">Jurusan</label>
            <input type="" class="form-control @error('jurusan') is-invalid @enderror" id="jurusan" placeholder="Jurusan" name="jurusan" value="{{$student->jurusan}}">
            @error('jurusan')<div class="invalid-feedback">{{$message}}</div>@enderror
          </div> --}}

          <div class="form-group">
            <label>Jurusan</label>
            <select name="jurusan" class="form-control select2" style="width: 40%;">
              @foreach ($jurusan as $jrs)
              <option value="{{$jrs->kode_jurusan}}"> {{$jrs->nama_jurusan}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label>Kelas</label>
            <select name="kelas_id" class="form-control select2" style="width: 60%;">
              @foreach ($kelas as $kls)
              <option value="{{$kls->kelas}}"> {{$kls->kelas}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label>Gelombang</label>
            <select name="gelombang_id" class="form-control select2" style="width: 60%;">
              @foreach ($gelombang as $glmbng)
              <option value="{{$glmbng->id}}"> {{$glmbng->gelombang}}</option>
              @endforeach
            </select>
          </div>
          
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>
    </form>
  </div>
  <!-- /.card -->

@endsection