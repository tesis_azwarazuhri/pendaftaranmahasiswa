@extends('layouts/main')

@section('title','Pendaftaran')
    
@section('container')


<div class="container">
    <div class="row">
        <div class="col">
            <h1>Data Entry </h1>
        </div>
    </div>

      <!-- Main content -->
  <!-- <section class="content">
  <div class="container-fluid">
  <h5 class="mb-2">Ekspedisi Detail</h5>
  </div>
  </section> -->

   <!-- /.row -->
   <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Ekspedisi Detail <i class="fas fa-plus" style="padding-left: 10px; color: red;" data-toggle="modal" data-target="#modal-xl"></i> </h3>
                <!-- <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl">
                  Launch Extra Large Modal
                </button> -->

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 350px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0"  style="height: 300px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th>No. Deliv Order</th>
                      <th>Tgl. Deliv Order</th>
                      <th>Ekspedisi</th>
                      <th>Resi</th>
                      <th>Tgl Resi</th>

                      <!-- tambahan -->
                      <th>Biaya Pengiriman</th>
                      <th>Status Pengiriman</th>
                      <th>Action</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-success">12111111</span></td>
                      <td>Done</td>

                      <td><span class="tag tag-success">150000</span></td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>
                    </tr>
                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-warning">12111111</span></td>
                      <td>Done</td>

                      <td>150000</td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>


                    </tr>
                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-warning">12111111</span></td>
                      <td>Done</td>

                      <td>150000</td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>
                    </tr>
                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-warning">12111111</span></td>
                      <td>Done</td>

                      <td>150000</td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>
                    </tr>

                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-warning">12111111</span></td>
                      <td>Done</td>

                      <td>150000</td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>
                    </tr>

                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-warning">12111111</span></td>
                      <td>Done</td>

                      <td>150000</td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>
                    </tr>

                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-warning">12111111</span></td>
                      <td>Done</td>

                      <td>150000</td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>
                    </tr>

                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-warning">12111111</span></td>
                      <td>Done</td>

                      <td>150000</td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>
                    </tr>


                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-warning">12111111</span></td>
                      <td>Done</td>

                      <td>150000</td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>
                    </tr>

                    <tr>
                      <td>183</td>
                      <td>12-12-2020</td>
                      <td>JNE</td>
                      <td><span class="tag tag-warning">12111111</span></td>
                      <td>Done</td>

                      <td>150000</td>
                      <td>Done</td>
                      <td>
                        <i class="fas fa-eye" style="padding: 2px; color: blue;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="View"></i>  
                        <i class="fas fa-edit" style="padding: 2px; color: green;" data-toggle="modal" data-target="#modal-xl-view"  data-toggle="tooltip" data-html="true" title="Edit"></i>  
                        <i class="fas fa-trash" style="padding: 2px; color: red;" data-toggle="modal" data-target="#modal-xl-delete"  data-toggle="tooltip" data-html="true" title="Hapus"></i> 
                     </td>
                    </tr>
                  </tbody>
                </table>
              </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          
        </div>
        <!-- /.row -->
   </div>




<div class="modal fade" id="modal-xl">

@endsection