@extends('layouts/main')

@section('title','Isi Data Calon Mahasiswa')
    
@section('container')

<div class="card card-primary ">
    <div class="card-header">
      <h3 class="card-title">Edit Data Biaya</h3>
    </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form method="POST" action="/classes/{{$classes->id}}">
                    @method('patch')
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="kelas">Kelas</label>
                            <input type="" class="form-control  @error('kelas') is-invalid @enderror" id="gelombang" placeholder="kelas" name="kelas" value="{{$classes->kelas}}">
                            @error('kelas')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>

                        <div class="form-group">
                            <label for="uangSpp">Uang SPP</label>
                            <input type="number" class="form-control  @error('uangSpp') is-invalid @enderror" id="uangSpp" placeholder="Rp." name="uangSpp" value="{{$classes->uang_spp}}">
                            @error('uangSpp')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>
                </div>
                
            </div>
        </div>
</div>
@endsection