@extends('layouts/main')

@section('title','Isi Data Calon Mahasiswa')
    
@section('container')

<div class="card card-primary ">
    <div class="card-header">
      <h3 class="card-title">Isi Data Biaya</h3>
    </div>
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <form method="POST" action="/classes">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="kelas">Kelas</label>
                            <input type="" class="form-control  @error('kelas') is-invalid @enderror" id="kelas" placeholder="kelas" name="kelas" value="{{old('kelas')}}">
                            @error('kelas')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>

                        <div class="form-group">
                            <label for="uangSpp">Uang SPP</label>
                            <input type="" class="form-control  @error('uangSpp') is-invalid @enderror" id="uangSpp" placeholder="Rp." name="uangSpp" value="{{old('uangSpp')}}">
                            @error('uangSpp')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>

                        {{-- <div class="form-group">
                            <label for="uangGedung">Uang Gedung</label>
                            <input type="number" class="form-control  @error('uangGedung') is-invalid @enderror" id="uangGedung" placeholder="Rp." name="uangGedung" value="{{old('uangGedung')}}">
                            @error('uangPendaftaran')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div> --}}
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>
                </div>
                
            </div>
        </div>
</div>
@endsection